// q1
SELECT DISTINCT replacement_cost 
FROM film;

// q2
SELECT COUNT(DISTINCT replacement_cost)
FROM film;

// q3
SELECT COUNT (*) 
from film
WHERE title 
LIKE 'T%'
AND
rating = 'G';

// q4
SELECT COUNT (*) 
from country
WHERE country 
LIKE '_____';

// q5
SELECT COUNT (*) 
from city
WHERE city 
ILIKE '%R';